rfpitols <- c('1e-7', '2e-7', '3e-7', '5e-7',
              '10e-7', '20e-7', '30e-7', '50e-7',
              '100e-7', '200e-7', '300e-7', '500e-7',
              '1000e-7', '2000e-7', '3000e-7', '5000e-7',
              '10000e-7', '20000e-7', '30000e-7', '50000e-7',
              '100000e-7')
numEntries <- length(rfpitols)

data <- data.frame(row.names = rfpitols,
                   tol = rep(NA, numEntries),
                   time = rep(NA, numEntries),
                   fpi = rep(NA, numEntries),
                   mg = rep(NA, numEntries))

directories <- ini::read.ini('config.ini')$directories
dir.create(directories[['output']], recursive=TRUE, showWarnings=FALSE)
for (rfpitol in rfpitols) {
  basedir <- paste('rfpitol', rfpitol, sep='=')
  dir     <- file.path(directories[['simulation']],
                       '2d-lab-fpi-tolerance', basedir)
  h5file  <- h5::h5file(file.path(dir, 'output.h5'), 'r')

  data[rfpitol,'tol'] <- as.numeric(rfpitol)

  relativeTimeProxy <- h5file['/relativeTime']
  relativeTimeLen <- relativeTimeProxy@dim
  data[rfpitol,'time'] <- relativeTimeProxy[relativeTimeProxy@dim]

  ## FIXME: why do we drop the first entry?
  fixedPointIterationsProxy <- h5file["/iterations/fixedPoint/total"]
  fixedPointIterationsLen <- fixedPointIterationsProxy@dim
  data[rfpitol,'fpi'] <- sum(fixedPointIterationsProxy[2:fixedPointIterationsLen])

  multiGridIterationsProxy <- h5file["/iterations/multiGrid/total"]
  multiGridIterationsLen <- multiGridIterationsProxy@dim
  data[rfpitol,'mg'] <- sum(multiGridIterationsProxy[2:multiGridIterationsLen])
  h5::h5close(h5file)
}

write.csv(data, file.path(directories[['output']], 'fpi-data.csv'),
          row.names = FALSE, quote = FALSE)
