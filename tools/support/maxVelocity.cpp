#include <Rcpp.h>

// For std::hypot(x,y)
// [[Rcpp::plugins(cpp11)]]

// Layout of vectors is such that loops over inner indices are traversed first
// [[Rcpp::export]]
Rcpp::NumericVector maxVelocity(Rcpp::NumericVector const &x) {
  Rcpp::IntegerVector size = x.attr("dim");
  Rcpp::NumericVector ret(size[0]);
  switch (size[2]) {
  case 1:
    for (size_t ts = 0; ts < size[0]; ++ts)
      for (size_t coord = 0; coord < size[1]; ++coord)
        ret[ts] = std::max(ret[ts],
                           std::abs(x[0 * size[1] * size[0] + coord * size[0] + ts]));
    break;
  case 2:
    for (size_t ts = 0; ts < size[0]; ++ts)
      for (size_t coord = 0; coord < size[1]; ++coord)
        ret[ts] = std::max(ret[ts],
                           std::hypot(x[0 * size[1] * size[0] + coord * size[0] + ts],
                                      x[1 * size[1] * size[0] + coord * size[0] + ts]));
    break;
  default:
    throw std::range_error("Inadmissible value");
  }
  return ret;
}
