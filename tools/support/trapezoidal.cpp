#include <Rcpp.h>

// [[Rcpp::export]]
double trapezoidal(Rcpp::NumericVector const &x, Rcpp::NumericVector const &y) {
  double ret = 0;
  for (size_t i = 1; i < x.size(); ++i)
    ret += (x[i] - x[i - 1]) * (y[i] + y[i - 1]) / 2;
  return ret;
}
