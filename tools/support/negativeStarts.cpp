#include <Rcpp.h>

// [[Rcpp::export]]
Rcpp::NumericVector negativeStarts(Rcpp::NumericVector const &x) {
  std::vector<size_t> starts(0);
  bool prev = false;
  for (size_t i = 0; i < x.size(); ++i) {
    if (prev && !x[i])
      starts.push_back(i+1); // R indices start at 1
    prev = x[i];
  }
  return Rcpp::NumericVector(starts.begin(), starts.end());
}
