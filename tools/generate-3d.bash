#!/usr/bin/env bash

set -e

rr() {
    echo "$(date)" Running: Rscript $@
    Rscript --vanilla --default-packages=grDevices,methods,stats,utils tools/$@
}

# performance
rr 3d-performance.R

# velocity contours
rr 3d-velocity-contours.R

date
