source('tools/support/findQuakes.R')

finalTime           <- 1000 # s
convergenceVelocity <- 5e-5 # m/s

paste.    <- function(...) paste(..., sep='.')
pasteColon<- function(...) paste(..., sep=':')

directories <- ini::read.ini('config.ini')$directories
dir.create(directories[['output']], recursive=TRUE, showWarnings=FALSE)
for (basedir in c("rfpitol=100e-7")) {
  dir          <- file.path(directories[['simulation']],
                            '2d-lab-fpi-tolerance', basedir)
  h5file       <- h5::h5file(file.path(dir, 'output.h5'), 'r')
  relativeTime <- h5file['relativeTime'][]
  realTime     <- finalTime * relativeTime

  velocityProxy<- h5file['/frictionalBoundary/velocity']

  ## We are interested in an enlarged time range around actual events here,
  ## (and no other quantities!) hence we pass a very low velocity here.
  quakes <- findQuakes(1e-6 + convergenceVelocity, velocityProxy,
                       indices = 1:dim(velocityProxy)[1], 1)
  quakes$beginning <- realTime[quakes$beginningIndex]
  quakes$ending    <- realTime[quakes$endingIndex]
  quakes$duration  <- quakes$ending - quakes$beginning
  numQuakes        <- nrow(quakes)

  relaxedTime <- extendrange(c(quakes[[numQuakes-2,'beginning']],
                               quakes[[numQuakes,  'ending']]), f=0.02)
  threeQuakeTimeMask <- (realTime > relaxedTime[[1]]) & (realTime < relaxedTime[[2]])
  plotMask   <- threeQuakeTimeMask
  plotIndices<- which(plotMask)
  plotIndices<- c(plotIndices[1]-1,plotIndices,plotIndices[length(plotIndices)]+1)

  write(relaxedTime[[1]],
        file.path(directories[['output']],
                  paste.(pasteColon('timeframe', 'min', 'threequakes',
                                    basedir), 'tex')))
  write(relaxedTime[[2]],
        file.path(directories[['output']],
                  paste.(pasteColon('timeframe', 'max', 'threequakes',
                                    basedir), 'tex')))

  timeWindow = realTime[plotIndices]

  relativeTau          <- h5file['relativeTimeIncrement'][]
  fixedPointIterations <- h5file['/iterations/fixedPoint/final'][]
  multiGridIterations  <- h5file['/iterations/multiGrid/final'][]
  write.csv(data.frame(time = timeWindow,
                       timeIncrement = finalTime * relativeTau[plotIndices],
                       fixedPointIterations = fixedPointIterations[plotIndices],
                       multiGridIterations = multiGridIterations[plotIndices]),
            file.path(directories[['output']],
                      paste.(pasteColon('2d-performance', basedir), 'csv')),
            row.names = FALSE, quote = FALSE)
  h5::h5close(h5file)
}
