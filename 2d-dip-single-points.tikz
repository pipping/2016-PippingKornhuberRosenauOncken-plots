\def\fnamebase{dip-single-points}
\def\fname{rfpitol=100e-7}

\newread\threequakesminread
\openin\threequakesminread=generated/timeframe:min:threequakes:\fname.tex
\read\threequakesminread to \threequakesmin
\closein\threequakesminread

\newread\threequakesmaxread
\openin\threequakesmaxread=generated/timeframe:max:threequakes:\fname.tex
\read\threequakesmaxread to \threequakesmax
\closein\threequakesmaxread

\begin{tikzpicture}[trim axis left, trim axis right]
  \begin{axis}[
    xmin=\threequakesmin, xmax=\threequakesmax, max space between ticks=40pt,
    /pgf/number format/1000 sep={},
    x tick label style={
      /pgf/number format/.cd,
      1000 sep={},
      /tikz/.cd
    },
    y tick label style={
      /pgf/number format/.cd,
      fixed, fixed zerofill, precision=2,
      /tikz/.cd
    },
    tick label style={font=\footnotesize},
    label style={font=\small},
    legend style={font=\small, at={(1.05,1)},
                  anchor=north west,
                  fill=none},
    legend entries={
      \SI{15}{\centi\meter},
      \SI{30}{\centi\meter},
      \SI{45}{\centi\meter}
    },
    ylabel style={align=center}, ylabel=vertical surface\\displacement, y unit = m,
    change y base, y SI prefix=micro,
    xlabel = time, x unit=s,
    width = 12cm,
    height = 4cm,
    semithick]
    \addplot[width=2pt] table[col sep=comma, x index = 0, y index=1]
      {generated/\fnamebase:\fname.csv};
    \addplot[width=2pt, dashed] table[col sep=comma, x index = 0, y index=2]
      {generated/\fnamebase:\fname.csv};
    \addplot[width=2pt, dotted] table[col sep=comma, x index = 0, y index=3]
      {generated/\fnamebase:\fname.csv};
  \end{axis}
\end{tikzpicture}
